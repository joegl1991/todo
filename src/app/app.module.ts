import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToDoComponent } from './to-do/to-do.component';
import { NumeroDirective } from './directivas/numero.directive';
import { UpperDirective } from './directivas/upper.directive';
import { HeaderComponent } from './common/header/header.component';
import { HomeComponent } from './home/home.component';

import { routing } from './app.routes';
import { ListComponent } from './list/list.component';
import { ToDoFormComponent } from './to-do-form/to-do-form.component'
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    ToDoComponent,
    NumeroDirective,
    UpperDirective,
    HeaderComponent,
    HomeComponent,
    ListComponent,
    ToDoFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
