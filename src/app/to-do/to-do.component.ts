import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {

  
  toDos = [];
  show = false;
  idTodo: any;

  constructor(private servicio: TodoService, private route: ActivatedRoute) {
    //this.idTodo = this.route.snapshot.params.idTodo;
    this.route.params.subscribe((params: Params) => {
      this.idTodo = params['idTodo'];
      console.log(this.idTodo);
      this.getToDos();
    });
    
  }

  ngOnInit() {
    this.getToDos();
  }

  getToDos() {
    /*
    this.toDos = this.servicio.get();
    if (this.idTodo) {
      this.toDos = this.toDos.filter(i => i.id === this.idTodo);
    }*/

    this.servicio.get()
      .subscribe(response => {
        this.toDos = response;
        if (this.idTodo) {
          this.toDos = this.toDos.filter(x => x.id === this.idTodo);
        }
      }, error => {
        console.log(error);
      });
  }

  agregar(model) {
    this.servicio.insertar(model)
      .subscribe(response => {
        console.log(response);
        this.getToDos();
      }, error => {
        console.log(error);
      });
    this.toDos.push(model);
    this.showInput();
  }

  showInput() {
    this.show = !this.show;
  }

  realizado(i) {
    console.log(i);
    this.toDos[i].feRealizado = new Date();
    console.log(this.toDos[i]);
    this.servicio.actualizar(this.toDos[i])
      .subscribe(response => {
        console.log(response);
        this.getToDos();
      }, error => {
        console.log(error);
      });
  }

  eliminar(i) {
    //this.toDos.splice(i, 1);
    console.log(this.toDos[i]);
    this.servicio.eliminar(this.toDos[i]._id)
      .subscribe(response => {
        console.log(response);
        this.getToDos();
      }, error => {
        console.log(error);
      });
  }

}
