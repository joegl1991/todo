import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ToDoComponent } from './to-do/to-do.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'todo', component: ToDoComponent },
    { path: 'todo/:idTodo', component: ToDoComponent },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);