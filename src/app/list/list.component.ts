import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() toDos = [];
  @Output() realizado = new EventEmitter();
  @Output() eliminar = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onRealizado(i) {
    this.realizado.next(i);
  }

  onEliminar(i) {
    this.eliminar.next(i);
  }

}
