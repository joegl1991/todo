import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { ToDoItem } from '../model/to-do-item';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: Http) { }

  /*get(): Array<ToDoItem> {
    return [
      {
        id: '1',
        descripcion: 'Levantar',
        hora: 10,
        feRealizado: new Date()
      },
      {
        id: '2',
        descripcion: 'Levantar2',
        hora: 12,
        feRealizado: new Date()
      },
      {
        id: '3',
        descripcion: 'Levantar3',
        hora: 13
      },
    ]
  }*/

  get() {
    return this.http.get('/api/todo/')
      .pipe(map((response: Response) => response.json()));
  }

  insertar(model) {
    return this.http.post('/api/todo/', model)
      .pipe(map((response: Response) => response.json()));
  }

  actualizar(model) {
    return this.http.put('/api/todo/', model)
      .pipe(map((response: Response) => response.json()));
  }

  eliminar(model) {
    console.log(model);
    return this.http.delete(`/api/todo/${model}`)
      .pipe(map((response: Response) => response.json()));
  }
}
