import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appUpper]'
})
export class UpperDirective {

  constructor(private el: ElementRef) { }

  @HostListener('keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {

    this.el.nativeElement.value = this.el.nativeElement.value.toUpperCase();

  }

}
